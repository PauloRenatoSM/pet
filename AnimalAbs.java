package br.edu.cest.pet.entity;

import java.math.BigDecimal;

import br.edu.cest.pet.controle.EnumPeso;

public abstract class AnimalAbs implements AnimalIFC {
	private String nome;
	private BigDecimal peso;
	private String fala;
	
	
	public AnimalAbs(String nome) {
		this.nome = nome;
		// TODO Auto-generated constructor stub
	}
	public String toString () {
		StringBuffer sb = new StringBuffer();
		sb.append("Nome:" + getNome());
		sb.append("\nFala:" + getFala());
		sb.append("\nPatas:" + getLocomocao());
		return sb.toString();
	}

	public String getNome() {
		return nome;
	}

	public void Setnome(String nome) {
		this.nome =nome;
		
	}
	
	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public abstract EnumUnidade getUnidadePeso() {
		return EnumUnidade.GRAMA;
	}
	
//	public String getPeso (String un);
//	public BigDecimal getPeso (int un);
//	public BigDecimal getPeso (String un);
	
	public BigDecimal getPeso (EnumUnidade un) {
		if (un.equals('g'))
			return getPeso().multiply(BigDecimal.valueOf(1000));
		else return getPeso();
		
	}
	
}
